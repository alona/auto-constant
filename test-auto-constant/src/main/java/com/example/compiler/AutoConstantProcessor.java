package com.example.compiler;

import com.example.order.AutoConstantValue;
import com.example.order.annotation.AutoConstant;
import com.example.order.annotation.Strategy;
import com.example.order.handler.StrategyHandler;
import com.example.order.spi.StrategyHandlerContext;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.api.JavacTrees;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.TypeTag;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.tree.TreeTranslator;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.util.Names;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 处理类
 *
 * @author XSJ
 * @version 1.0
 */
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("com.example.order.annotation.AutoConstant")
public class AutoConstantProcessor extends AbstractProcessor {

    private Messager messager;
    private JavacTrees trees;
    private TreeMaker treeMaker;
    private Names names;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        this.messager = processingEnv.getMessager();
        this.trees = JavacTrees.instance(processingEnv);
        Context context = ((JavacProcessingEnvironment) processingEnv).getContext();
        this.treeMaker = TreeMaker.instance(context);
        this.names = Names.instance(context);
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(AutoConstant.class);
        for (Element element : elements) {
            JCTree jcTree = trees.getTree(element);
            jcTree.accept(new AutoConstantTreeTranslator(messager, trees, treeMaker, names));
        }
        return true;
    }

    private static class AutoConstantTreeTranslator extends TreeTranslator {

        private final Messager messager;
        private final JavacTrees trees;
        private final TreeMaker treeMaker;
        private final Names names;

        private AutoConstantTreeTranslator(Messager messager, JavacTrees trees, TreeMaker treeMaker, Names names) {
            this.messager = messager;
            this.trees = trees;
            this.treeMaker = treeMaker;
            this.names = names;
        }


        @Override
        public void visitClassDef(JCTree.JCClassDecl jcClassDecl) {
            super.visitClassDef(jcClassDecl);

            final String annotationName = AutoConstant.class.getName();

            JCTree.JCAnnotation jcAnnotation = getJcAnnotation(jcClassDecl, annotationName);

            System.out.format("-----------------visitClassDef %s-----------------\r\n", jcClassDecl.sym.toString());
            messager.printMessage(Diagnostic.Kind.NOTE, String.format("-----------------visitClassDef-----------------"));

            java.util.List<JCTree> jcTreeList = new ArrayList<>();
            for (JCTree def : jcClassDecl.defs) {
                if (def.getKind().equals(Tree.Kind.VARIABLE)) {
                    jcTreeList.add(def);
                }
            }

//            int value;
//            int step;
//            Strategy strategy;
//
//            List<JCTree.JCExpression> arguments = jcAnnotation.getArguments();
//            for (JCTree.JCExpression argument : arguments) {
//                Object methodValue = null;
//                JCTree.JCExpression rhs = ((JCTree.JCAssign) argument).rhs;
//
//                if (rhs instanceof JCTree.JCFieldAccess) {
//                    methodValue = Strategy.valueOf(((JCTree.JCFieldAccess) rhs).sym.toString());
//                } else if (rhs instanceof JCTree.JCLiteral) {
//                    methodValue = ((JCTree.JCLiteral) rhs).getValue();
//                }
//                String name = ((JCTree.JCIdent) ((JCTree.JCAssign) argument).lhs).getName().toString();
//                if (methodValue != null) {
//                    switch (name) {
//                        case "value":
//                            value = (Integer) methodValue;
//                            break;
//                        case "step":
//                            step = (Integer) methodValue;
//                            break;
//                        case "strategy":
//                            strategy = (Strategy) methodValue;
//                            break;
//                    }
//                }
//            }


            LinkedHashMap<String, Number> nameValueMap = new LinkedHashMap<>();
            Map<String, JCTree.JCExpression> nameTypeCastMap = new LinkedHashMap<>();

            for (JCTree jcTree : jcTreeList) {
                JCTree.JCVariableDecl jcVariableDecl = (JCTree.JCVariableDecl) jcTree;

                JCTree.JCExpression initE = jcVariableDecl.init;

                System.out.format("%50s = %s%n", jcVariableDecl.getName(), initE);

                JCTree.JCExpression vartype = jcVariableDecl.vartype;

//                // 对象
//                JCTree.JCIdent vartype1 = (JCTree.JCIdent) jcVariableDecl.vartype;
//                // 基础类型
//                JCTree.JCPrimitiveTypeTree vartype = (JCTree.JCPrimitiveTypeTree) jcVariableDecl.vartype;

                String name = jcVariableDecl.getName().toString();

                // 字段是数字类型的前提下
                // 如果有强制类型转换，如 int a = (int)1;，则 init 的类型是 JCTree.JCTypeCast
                // 否则 int a = 1; init的类型是 JCTree.JCLiteral
                if (initE == null) {

//                    TypeTag typetag = (vartype).typetag;
//                    jcVariableDecl.init = treeMaker.TypeCast(vartype, treeMaker.Literal(typetag, 0));
                    if (vartype instanceof JCTree.JCIdent) {
                        TypeTag typetag;
                        // 对象
                        switch (vartype.toString()){
                            case "Byte":
                                typetag = TypeTag.BYTE;
                                break;
                            case "Short":
                                typetag = TypeTag.SHORT;
                                break;
                            case "Integer":
                                typetag = TypeTag.INT;
                                break;
                            case "Long":
                                typetag = TypeTag.LONG;
                                break;
                            case "Double":
                                typetag = TypeTag.DOUBLE;
                                break;
                            case "Float":
                                typetag = TypeTag.FLOAT;
                                break;
                            case "Character":
                                typetag = TypeTag.CHAR;
                                break;
                            default:
                                throw new RuntimeException("No SuchClass");
                        }

                        JCTree.JCPrimitiveTypeTree jcPrimitiveTypeTree = treeMaker.TypeIdent(typetag);
                        jcVariableDecl.init = treeMaker.TypeCast(jcPrimitiveTypeTree, treeMaker.Literal(TypeTag.INT, 0));

                    } else if (vartype instanceof JCTree.JCPrimitiveTypeTree) {
                        // 基本类型
                        TypeTag typetag = ((JCTree.JCPrimitiveTypeTree) vartype).typetag;
                        jcVariableDecl.init = treeMaker.TypeCast(vartype, treeMaker.Literal(typetag, 0));

                    } else if (vartype instanceof JCTree.JCFieldAccess) {
                        TypeTag typetag;
                        switch (((JCTree.JCFieldAccess) vartype).name.toString()){
                            case "Byte":
                                typetag = TypeTag.BYTE;
                                break;
                            case "Short":
                                typetag = TypeTag.SHORT;
                                break;
                            case "Integer":
                                typetag = TypeTag.INT;
                                break;
                            case "Long":
                                typetag = TypeTag.LONG;
                                break;
                            case "Double":
                                typetag = TypeTag.DOUBLE;
                                break;
                            case "Float":
                                typetag = TypeTag.FLOAT;
                                break;
                            case "Character":
                                typetag = TypeTag.CHAR;
                                break;
                            default:
                                throw new RuntimeException("No SuchClass");
                        }
                        JCTree.JCFieldAccess jcFieldAccess = (JCTree.JCFieldAccess) vartype;
                        JCTree.JCPrimitiveTypeTree jcPrimitiveTypeTree = treeMaker.TypeIdent(typetag);
                        jcVariableDecl.init = treeMaker.TypeCast(jcPrimitiveTypeTree, treeMaker.Literal(typetag, 0));
                    }

                    nameValueMap.put(name, null);
                    nameTypeCastMap.put(name, jcVariableDecl.init);

                } else if (initE instanceof JCTree.JCTypeCast) {

                    JCTree.JCTypeCast init = (JCTree.JCTypeCast) jcVariableDecl.init;
                    JCTree.JCLiteral expr = (JCTree.JCLiteral) init.expr;
                    nameValueMap.put(name, (Number) expr.value);
                    nameTypeCastMap.put(name, init);

                } else if (initE instanceof JCTree.JCLiteral) {

                    JCTree.JCLiteral init = (JCTree.JCLiteral) jcVariableDecl.init;
                    Object value = ((JCTree.JCLiteral) ((JCTree.JCVariableDecl) jcTreeList.get(2)).init).getValue();
                    nameValueMap.put(name, (Number) value);
                    nameTypeCastMap.put(name, init);

                }

//                TypeTag typeTag = expr.typetag;
//                // 重新赋值
//                init.expr = treeMaker.Literal(typeTag, 666);
            }

            AutoConstantValue annotationValue = getAnnotationValue(jcAnnotation);

            // 自定义处理器
            StrategyHandler handler = StrategyHandlerContext.getHandler(annotationValue.getStrategy());
            LinkedHashMap<String, Number> resultMap = handler.handle(nameValueMap, annotationValue.getValue(), annotationValue.getStep(), annotationValue.getStrategy());

            for (Map.Entry<String, JCTree.JCExpression> entry : nameTypeCastMap.entrySet()) {
                JCTree.JCExpression initE = entry.getValue();


                if (initE instanceof JCTree.JCTypeCast) {
                    JCTree.JCTypeCast init = (JCTree.JCTypeCast) initE;
                    JCTree.JCLiteral expr = (JCTree.JCLiteral) (init).expr;
                    TypeTag typeTag = expr.typetag;
                    // 重新赋值
//                    System.out.println(init);
                    // 这样不行
//                    init.expr = treeMaker.Literal(typeTag, resultMap.get(entry.getKey()));
                    // 这样也不行
//                    expr.value = resultMap.get(entry.getKey());
                    // 这样才行
                    init.expr = treeMaker.Literal(resultMap.get(entry.getKey()));

                } else if (initE instanceof JCTree.JCLiteral) {
                    JCTree.JCLiteral init = (JCTree.JCLiteral) initE;
                    // 重新赋值
                    init.value = resultMap.get(entry.getKey());
                }
            }

//            for (JCTree jcTree : jcTreeList) {
//                JCTree.JCVariableDecl jcVariableDecl = (JCTree.JCVariableDecl) jcTree;
//
//                messager.printMessage(Diagnostic.Kind.NOTE, jcVariableDecl.getName() + " has been processed");
//
//                JCTree.JCTypeCast init = (JCTree.JCTypeCast) jcVariableDecl.init;
//
//                System.out.format("%20s = %s%n", jcVariableDecl.getName(), init);
//
//                TypeTag typeTag = ((JCTree.JCLiteral) init.expr).typetag;
//
//                // 重新赋值
//                init.expr = treeMaker.Literal(typeTag, 2);
////                // 构建一个字面量数值
////                System.out.println("treeMaker.Literal((byte) 2) = " + treeMaker.Literal((byte) 2));
////                System.out.println("treeMaker.Literal   = " + treeMaker.Literal(TypeTag.BYTE, 2));
//                System.out.println("------------------------");
////                JCTree.JCVariableDecl jcVariableDecl1 = treeMaker.VarDef(
////                        treeMaker.Modifiers(Flags.PRIVATE),
////                        names.fromString("name"),
////                        treeMaker.Ident(names.fromString("String")),
////                        treeMaker.Literal(TypeTag.BYTE, 1));
////
////                JCTree.JCVariableDecl jcVariableDecl1 = treeMaker.VarDef(
////                        jcVariableDecl.getModifiers(),
////                        jcVariableDecl.getName(),
////                        jcVariableDecl.vartype,
////                        treeMaker.Literal(TypeTag.BYTE, 2));
////                System.out.println("jcVariableDecl1 = " + jcVariableDecl1);
////
////                jcClassDecl.defs = jcClassDecl.defs.prepend(makeGetterMethodDecl(jcVariableDecl));
//            }

        }

    }

    private static JCTree.JCAnnotation getJcAnnotation(JCTree.JCClassDecl jcClassDecl, String annotationName) {
        JCTree.JCAnnotation jcAnnotation = null;
        for (JCTree.JCAnnotation a : jcClassDecl.mods.getAnnotations()) {
            if (a.type.toString().equals(annotationName)) {
                jcAnnotation = a;
                break;
            }
        }
        return jcAnnotation;
    }

    private static AutoConstantValue getAnnotationValue(JCTree.JCAnnotation jcAnnotation) {

        AutoConstantValue annotationValue = new AutoConstantValue();

        List<JCTree.JCExpression> arguments = jcAnnotation.getArguments();
        for (JCTree.JCExpression argument : arguments) {
            Object methodValue = null;
            JCTree.JCExpression rhs = ((JCTree.JCAssign) argument).rhs;

            if (rhs instanceof JCTree.JCFieldAccess) {
                methodValue = Strategy.valueOf(((JCTree.JCFieldAccess) rhs).sym.toString());
            } else if (rhs instanceof JCTree.JCLiteral) {
                methodValue = ((JCTree.JCLiteral) rhs).getValue();
            }
            String name = ((JCTree.JCIdent) ((JCTree.JCAssign) argument).lhs).getName().toString();
            if (methodValue != null) {
                switch (name) {
                    case "value":
                        annotationValue.setValue((Integer) methodValue);
                        break;
                    case "step":
                        annotationValue.setStep((Integer) methodValue);
                        break;
                    case "strategy":
                        annotationValue.setStrategy((Strategy) methodValue);
                        break;
                }
            }
        }
        return annotationValue;
    }
//    private JCTree.JCMethodDecl makeGetterMethodDecl(JCTree.JCVariableDecl jcVariableDecl) {
//        ListBuffer<JCTree.JCStatement> statements = new ListBuffer<>();
//        statements.append(treeMaker.Return(treeMaker.Select(treeMaker.Ident(names.fromString("this")), jcVariableDecl.getName())));
//        JCTree.JCBlock body = treeMaker.Block(0, statements.toList());
//        return treeMaker.MethodDef(treeMaker.Modifiers(Flags.PUBLIC), getNewMethodName(jcVariableDecl.getName()), jcVariableDecl.vartype, List.nil(), List.nil(), List.nil(), body, null);
//    }
//
//    private Name getNewMethodName(Name name) {
//        String s = name.toString();
//        return names.fromString("get" + s.substring(0, 1).toUpperCase() + s.substring(1, name.length()));
//    }

}
