package com.example.order.constant;

import com.example.order.annotation.AutoConstant;
import com.example.order.annotation.Strategy;

/**
 * @author XSJ
 * @version 1.0
 */
@AutoConstant(step = 1, strategy = Strategy.DECREMENTAL)
public class AopOrderConstant {
    public static final byte STATIC_FINAL_BYTE = (byte)1;
    public static byte STATIC_BYTE = (byte)1;
    public final byte FINAL_BYTE = (byte)1;
    public byte BYTE = (byte)1;
    public byte NULL_BYTE;

    public static final java.lang.Byte STATIC_FINAL__BYTE = (byte)1;
    public static java.lang.Byte STATIC__BYTE = (byte)1;
    public final java.lang.Byte FINAL__BYTE = (byte)1;
    public java.lang.Byte _BYTE = (byte)1;
    public java.lang.Byte NULL__BYTE;

    public static final short STATIC_FINAL_SHORT = (short)1;
    public static short STATIC_SHORT = (short)1;
    public final short FINAL_SHORT = (short)1;
    public short SHORT = (short)1;
    public short NULL_SHORT;

    public static final java.lang.Short STATIC_FINAL__SHORT = (short)1;
    public static java.lang.Short STATIC__SHORT = (short)1;
    public final java.lang.Short FINAL__SHORT = (short)1;
    public java.lang.Short _SHORT = (short)1;
    public java.lang.Short NULL__SHORT;

    public static final int STATIC_FINAL_INT = (int)1;
    public static int STATIC_INT = (int)1;
    public final int FINAL_INT = (int)1;
    public int INT = (int)1;
    public int NULL_INT;

    public static final java.lang.Integer STATIC_FINAL__INTEGER = (int)1;
    public static java.lang.Integer STATIC__INTEGER = (int)1;
    public final java.lang.Integer FINAL__INTEGER = (int)1;
    public java.lang.Integer _INTEGER = (int)1;
    public java.lang.Integer NULL__INTEGER;

    public static final float STATIC_FINAL_FLOAT = (float)1;
    public static float STATIC_FLOAT = (float)1;
    public final float FINAL_FLOAT = (float)1;
    public float FLOAT = (float)1;
    public float NULL_FLOAT;

    public static final java.lang.Float STATIC_FINAL__FLOAT = (float)1;
    public static java.lang.Float STATIC__FLOAT = (float)1;
    public final java.lang.Float FINAL__FLOAT = (float)1;
    public java.lang.Float _FLOAT = (float)1;
    public java.lang.Float NULL__FLOAT;

    public static final double STATIC_FINAL_DOUBLE = (double)1;
    public static double STATIC_DOUBLE = (double)1;
    public final double FINAL_DOUBLE = (double)1;
    public double DOUBLE = (double)1;
    public double NULL_DOUBLE;

    public static final java.lang.Double STATIC_FINAL__DOUBLE = (double)1;
    public static java.lang.Double STATIC__DOUBLE = (double)1;
    public final java.lang.Double FINAL__DOUBLE = (double)1;
    public java.lang.Double _DOUBLE = (double)1;
    public java.lang.Double NULL__DOUBLE;

    public static final long STATIC_FINAL_LONG = (long)1;
    public static long STATIC_LONG = (long)1;
    public final long FINAL_LONG = (long)1;
    public long LONG = (long)1;
    public long NULL_LONG;

    public static final java.lang.Long STATIC_FINAL__LONG = (long)1;
    public static java.lang.Long STATIC__LONG = (long)1;
    public final java.lang.Long FINAL__LONG = (long)1;
    public java.lang.Long _LONG = (long)1;
    public java.lang.Long NULL__LONG;

    public static final char STATIC_FINAL_CHAR = (char)1;
    public static char STATIC_CHAR = (char)1;
    public final char FINAL_CHAR = (char)1;
    public char CHAR = (char)1;
    public char NULL_CHAR;

    public static final java.lang.Character STATIC_FINAL__CHARACTER = (char)1;
    public static java.lang.Character STATIC__CHARACTER = (char)1;
    public final java.lang.Character FINAL__CHARACTER = (char)1;
    public java.lang.Character _CHARACTER = (char)1;
    public java.lang.Character NULL__CHARACTER;



}
