package com.example.order.spi;

import com.example.order.annotation.Strategy;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StrategyHandlerContextTest {

    @Test
    void getHandler() {
        assertNotNull(StrategyHandlerContext.getHandler(Strategy.INCREMENTAL));
        assertNotNull(StrategyHandlerContext.getHandler(Strategy.DISCONTINUOUS_INCREMENT));
        assertNotNull(StrategyHandlerContext.getHandler(Strategy.DECREMENTAL));
        assertNotNull(StrategyHandlerContext.getHandler(Strategy.DISCONTINUOUS_DECREMENTAL));
    }

}