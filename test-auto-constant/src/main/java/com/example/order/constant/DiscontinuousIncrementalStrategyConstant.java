package com.example.order.constant;

import com.example.order.annotation.AutoConstant;
import com.example.order.annotation.Strategy;

/**
 * @author 孤胆枪手
 * @version 1.0
 */
@AutoConstant(strategy = Strategy.DISCONTINUOUS_INCREMENT)
public class DiscontinuousIncrementalStrategyConstant {
    public static int c1;
    public static int c2;
    public static int c3 = 0;
    public static int c4;
    public static int c5;
    public static int c6;
    public static int c7;
}
