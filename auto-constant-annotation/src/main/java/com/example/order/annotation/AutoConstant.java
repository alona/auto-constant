package com.example.order.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface AutoConstant {
    /**
     * 初始值
     *
     * @return 默认 0
     */
    int value() default 0;

    /**
     * 步长
     *
     * @return int值
     */
    int step() default 1;

    /**
     * 策略
     *
     * @return {@link com.example.order.annotation.Strategy}
     */
    Strategy strategy() default Strategy.INCREMENTAL;
}