package com.example.order.handler;

import com.example.order.annotation.Strategy;
import com.google.auto.service.AutoService;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 孤胆枪手
 * @version 1.0
 */
@AutoService({StrategyHandler.class})
public class DecrementalStrategyHandler implements StrategyHandler {

    @Override
    public Strategy support() {
        return Strategy.DECREMENTAL;
    }

    @Override
    public LinkedHashMap<String, Number> handle(final LinkedHashMap<String, Number> nameValueMap, final int initValue,
                                                final int step, final Strategy strategy) {
        int i = initValue;
        LinkedHashMap<String, Number> result = new LinkedHashMap<>();
        for (Map.Entry<String, Number> entry : nameValueMap.entrySet()) {
            String name = entry.getKey();
            result.put(name, i--);
        }
        return result;
    }
}
