package com.example.compiler;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.text.MessageFormat;

/**
 * @author 孤胆枪手
 * @version 1.0
 */
public class MockTest {

    @Test
    public void test1() {
        Class<?>[] classes = {
                byte.class,
                Byte.class,
                short.class,
                Short.class,
                int.class,
                Integer.class,
                float.class,
                Float.class,
                double.class,
                Double.class,
                long.class,
                Long.class,
                char.class,
                Character.class
        };
//        String s = "    public static final {0} STATIC_FINAL_{1} = {2};\n" +
//                "    public static {0} STATIC_{1} = {2};\n" +
//                "    public final {0} FINAL_{1} = {2};\n" +
//                "    public {0} {1} = {2};\n"+
//                "    public {0} NULL_{1};\n";
        String s =
                "    public {0} _{1} = {2};\n"+
                "    public {0} _VALUE_OF_{1} = {0}.valueOf({2});\n"+
                "    public {0} _NULL_{1};\n";

        for (int i = 0; i < classes.length; i += 2) {
            Class<?> baseClass = classes[i];
            String baseClassName = baseClass.getName();
            String baseValue = "(" + baseClassName + ")1";

            System.out.println(MessageFormat.format(s, baseClassName, baseClassName.toUpperCase(), baseValue));

            Class<?> boxClass = classes[i + 1];
            String boxClassName = boxClass.getName();
            int indexOf = boxClassName.lastIndexOf('.');
            String fieldName = '_' + boxClassName.substring(indexOf + 1).toUpperCase();
            System.out.println(MessageFormat.format(s, boxClassName, fieldName, baseValue));
        }
    }
}
