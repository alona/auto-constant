package com.example.order.constant;

import com.example.order.annotation.AutoConstant;
import com.example.order.annotation.Strategy;

/**
 * @author XSJ
 * @version 1.0
 */
@AutoConstant(step = 1, strategy = Strategy.DECREMENTAL)
public class NumberClassConstant {
    public byte _BYTE = (byte) 1;
    public byte _NULL_BYTE;

    public Byte __BYTE = (byte) 1;
//    public Byte _VALUE_OF__BYTE = Byte.valueOf((byte) 1);
    public Byte _NULL__BYTE;

    public short _SHORT = (short) 1;
    public short _NULL_SHORT;

    public Short __SHORT = (short) 1;
//    public Short _VALUE_OF__SHORT = Short.valueOf((short) 1);
    public Short _NULL__SHORT;

    public int _INT = (int) 1;
    public int _NULL_INT;

    public Integer __INTEGER = (int) 1;
//    public Integer _VALUE_OF__INTEGER = Integer.valueOf((int) 1);
    public Integer _NULL__INTEGER;

    public float _FLOAT = (float) 1;
    public float _NULL_FLOAT;

    public Float __FLOAT = (float) 1;
//    public Float _VALUE_OF__FLOAT = Float.valueOf((float) 1);
    public Float _NULL__FLOAT;

    public double _DOUBLE = (double) 1;
    public double _NULL_DOUBLE;

    public Double __DOUBLE = (double) 1;
//    public Double _VALUE_OF__DOUBLE = Double.valueOf((double) 1);
    public Double _NULL__DOUBLE;

    public long _LONG = (long) 1;
    public long _NULL_LONG;

    public Long __LONG = (long) 1;
//    public Long _VALUE_OF__LONG = Long.valueOf((long) 1);
    public Long _NULL__LONG;

    public char _CHAR = (char) 1;
    public char _NULL_CHAR;

    public Character __CHARACTER = (char) 1;
//    public Character _VALUE_OF__CHARACTER = Character.valueOf((char) 1);
    public Character _NULL__CHARACTER;


}
