package com.example.order.spi;

import com.example.order.annotation.Strategy;
import com.example.order.handler.*;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;

/**
 * @author 孤胆枪手
 * @version 1.0
 */
public class StrategyHandlerContext {

    private static final Map<Strategy, StrategyHandler> HANDLER_MAP = new HashMap<>();

    static {
        ServiceLoader<StrategyHandler> loader = ServiceLoader.load(StrategyHandler.class);
        for (StrategyHandler handler : loader) {
            HANDLER_MAP.put(handler.support(), handler);
        }
        HANDLER_MAP.put(Strategy.DECREMENTAL, new DecrementalStrategyHandler());
        HANDLER_MAP.put(Strategy.DISCONTINUOUS_DECREMENTAL, new DiscontinuousDecrementalStrategyHandler());
        HANDLER_MAP.put(Strategy.INCREMENTAL, new IncrementalStrategyHandler());
        HANDLER_MAP.put(Strategy.DISCONTINUOUS_INCREMENT, new DiscontinuousIncrementalStrategyHandler());
    }

    public static StrategyHandler getHandler(Strategy strategy) {
        return HANDLER_MAP.get(strategy);
    }

}
