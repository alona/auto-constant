package com.example.order;

import com.example.order.annotation.Strategy;

public class AutoConstantValue {

    private int value = 0;

    private int step = 1;

    private Strategy strategy = Strategy.INCREMENTAL;

    public AutoConstantValue() {
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public int getValue() {
        return value;
    }

    public int getStep() {
        return step;
    }

    public Strategy getStrategy() {
        return strategy;
    }
}
