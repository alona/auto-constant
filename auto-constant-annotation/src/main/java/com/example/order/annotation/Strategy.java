package com.example.order.annotation;

/**
 * 策略
 */
public enum Strategy {
    /**
     * 递增
     */
    INCREMENTAL,
    /**
     * 不连续递增
     */
    DISCONTINUOUS_INCREMENT,
    /**
     * 递减
     */
    DECREMENTAL,
    /**
     * 不连续递减
     */
    DISCONTINUOUS_DECREMENTAL,
    ;
}