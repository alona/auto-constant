# auto-constant

#### 介绍

一个可以自动增长常量的工具，拒绝每次增加一个常量就需要人工赋递增的初值

#### 软件架构

软件架构说明

#### 安装教程

1. clone 代码
2. mvn clean package
3. ```java
    <dependency>
        <groupId>org.example</groupId>
        <artifactId>auto-constant</artifactId>
        <version>${auto-constant.version}</version>
    </dependency>
```

#### 使用说明

> 替代这个（写完了才想起来其实可以这么写哈哈哈哈）
```java
public class AutoConstant {
    public static final int STEP = 1;
    public static final int c1 = 0;
    public static final int c2 = c1 + STEP;
    public static final int c3 = c2 + STEP;
    public static final int c4 = c3 + STEP;
}
```

1. 引入jar包
    ```xml
    <dependency>
        <groupId>org.example</groupId>
        <artifactId>auto-constant-annotation</artifactId>
        <version>${auto-constant.version}</version>
    </dependency>
   ```
2. 在常量类上加 @AutoConstant 注解
    ```java
    @AutoConstant(step = 1, strategy = Strategy.INCREMENTAL)
    public class AopOrderConstant {
        public static final int A1 = 0;
        public static final int A2 = 0;
        public static final int A3 = 0;
    }
    ```
3. 编译一下，然后反编译 AopOrderConstant.class 得到 
    ```java
    public class AopOrderConstant {
        public static final int A1 = 0;
        public static final int A2 = 1;
        public static final int A3 = 2;
    }
    ```

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

# 参考文章

1. maven plugin 官方文档：https://maven.apache.org/guides/introduction/introduction-to-plugins.html
2. 编写 maven 插件：https://www.cnblogs.com/davenkin/p/advanced-maven-write-your-own-plugin.html
3. idea 加载 tools.jar 源码：https://blog.csdn.net/u014454538/article/details/122895584
4. tools.jar 源码：http://hg.openjdk.java.net/jdk8/jdk8/langtools
5. IDEA DEBUG：https://stackoverflow.com/questions/31345893/debug-java-annotation-processors-using-intellij-and-maven
6. Java 编译器 javac 笔记：javac API、注解处理 API 与 Lombok 原理：https://segmentfault.com/a/1190000019290953
7. 抽象语法树AST的全面解析（一）：https://www.jianshu.com/p/ff8ec920f5b9
8. 抽象语法树AST的全面解析（二）：https://www.jianshu.com/p/4bd5dc13f35a
9. 抽象语法树AST的全面解析（三）：https://www.jianshu.com/p/68fcbc154c2f
10. Java 中的屠龙之术：如何修改语法树？：https://segmentfault.com/a/1190000040786580
11. Lombok 原理：https://blog.mythsman.com/post/5d2c11c767f841464434a3bf/
12. lombok 原理：http://patamon.me/icemimosa/Java/[Lombok%E5%8E%9F%E7%90%863]%E5%AE%9E%E7%8E%B0%E4%B8%80%E4%B8%AA%E7%AE%80%E5%8D%95%E7%9A%84lombok/
13. 知识点汇总：如何修改Java的抽象语法树？ ：https://juejin.cn/post/6844904151873241095   
14. 

