package com.example.order.handler;

import com.example.order.annotation.Strategy;

import java.util.LinkedHashMap;

/**
 * @author XSJ
 * @version 1.0
 */
public interface StrategyHandler {

    Strategy support();

    LinkedHashMap<String, Number> handle(final LinkedHashMap<String, Number> nameValueMap, final int initValue, final int step, final Strategy strategy);
}
