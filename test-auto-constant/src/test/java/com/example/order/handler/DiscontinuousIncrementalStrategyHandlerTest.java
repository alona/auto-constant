package com.example.order.handler;

import com.example.order.annotation.Strategy;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.LinkedHashMap;

import static org.junit.jupiter.api.Assertions.*;

class DiscontinuousIncrementalStrategyHandlerTest {

    private static final StrategyHandler HANDLER = new DiscontinuousIncrementalStrategyHandler();
    @Test
    void support() {
        Strategy support = HANDLER.support();
        assertTrue(support.equals(Strategy.DISCONTINUOUS_INCREMENT));
    }

    @Test
    void handle() {
        final LinkedHashMap<String, Number> nameValueMap = new LinkedHashMap<>();
        nameValueMap.put("1", null);
        nameValueMap.put("2", null);
        nameValueMap.put("3", Integer.valueOf(0));
        nameValueMap.put("4", null);
        LinkedHashMap<String, Number> handle = HANDLER.handle(nameValueMap, 0, 1, Strategy.DISCONTINUOUS_INCREMENT);
        assertEquals(handle.get("1"), 0);
        assertEquals(handle.get("2"), 1);
        assertEquals(handle.get("3"), 0);
        assertEquals(handle.get("4"), 1);
    }
}