package com.example.compiler;


import org.junit.jupiter.api.Test;

import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author 孤胆枪手
 * @version 1.0
 */
class MyScannerProcessorTest {

    private static final String MODEL_FOLDER = "test-auto-constant/";
    private static final String FILE_PATH = "src/main/java/com/example/order/AopOrderConstant.java";
    private static final File[] FILES = new File("./src/main/java/com/example/order/constant").listFiles();
    private static final String CLASSES_PATH = "target/classes";

    @Test
    void test1() throws IOException {

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        MyScannerProcessor processor = new MyScannerProcessor();

        StandardJavaFileManager manager = compiler.getStandardFileManager(diagnostics, null, null);
        Iterable<? extends JavaFileObject> sources = manager.getJavaFileObjects(FILES);

        JavaCompiler.CompilationTask task = compiler.getTask(null, manager, diagnostics, Arrays.asList("-d", CLASSES_PATH), null, sources);
        task.setProcessors(Arrays.asList(processor));
        task.call();

        manager.close();
    }
}
